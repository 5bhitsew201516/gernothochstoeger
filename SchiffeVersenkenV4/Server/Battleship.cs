﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Battleship:Ship
    {
        public Battleship(List<string> allp):base(allp)
        {
            this.Size = 5;
            this.AllPositions = allp;
        }

    }
}
