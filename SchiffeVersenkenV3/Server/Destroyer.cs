﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Destroyer:Ship
    {
        public Destroyer(List<string> allp) : base(allp)
        {
            this.Size = 3;
            this.AllPositions = allp;
        }
    }
}
