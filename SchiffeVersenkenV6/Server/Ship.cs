﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Ship
    {
        public int Size;
        public List<string> AllPositions;
        public List<string> HitPositions = new List<string>();
        public int GradeOfDamage = 0;
        public Ship(List<string> allp)
        {
            AllPositions = allp;
        }

    }
}
