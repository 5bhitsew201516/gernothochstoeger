﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Player
    {
        static int nextID = 1;
        public int ID;
        public List<Ship> Ships;
        public Player()
        {
            ID = nextID;
            nextID++;
        }

        public void SetShips(Dictionary<string, List<string>> dicShips)
        {
            foreach (var item in dicShips)
            {
                switch (item.Key.Substring(0, 3))
                {
                    case "2er":
                        Submarine s = new Submarine(item.Value);
                        Ships.Add(s);
                        break;
                    case "3er":
                        Destroyer d = new Destroyer(item.Value);
                        Ships.Add(d);
                        break;
                    case "4er":
                        Cruiser c = new Cruiser(item.Value);
                        Ships.Add(c);
                        break;
                    case "5er":
                        Battleship b = new Battleship(item.Value);
                        Ships.Add(b);
                        break;
                }
            }
        }
    }
}
