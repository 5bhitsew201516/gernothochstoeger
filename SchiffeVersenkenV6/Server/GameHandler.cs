﻿using Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class GameHandler : MarshalByRefObject,ISchifferlService 
    {
        List<Game> Games = new List<Game>();
        public void setShips(Dictionary<string, List<string>> Ships, out int playerID)
        {
            Player player = new Player();
            playerID = player.ID;
            player.SetShips(Ships);
            if (Games.Count != 0)
            {
                if (Games.Last().Player2 == null)
                    Games.Last().Player2 = player;
                else
                {
                    Game game = new Game();
                    game.Player1 = player;
                    Games.Add(game);
                }
            }
            else
            {
                Game game = new Game();
                game.Player1 = player;
                Games.Add(game);
            }
            Console.WriteLine("Schiffe erhalten");
        }

        public bool hitField(string field,ref int tries, out bool gameFinished, out bool shipFinished, int id)
        {
            Player actPlayer = null;
            Player foreignPlayer = null;
            bool hit = false;
            int countOfHitShips = 0;
            shipFinished = false;
            for (int i = 0; i < Games.Count; i++)
            {
                if (Games[i].Player1.ID == id)
                {
                    actPlayer = Games[i].Player1;
                    foreignPlayer = Games[i].Player2;
                }
                if (Games[i].Player2.ID == id)
                {
                    actPlayer = Games[i].Player2;
                    foreignPlayer = Games[i].Player1;
                }
            }

            foreach (Ship s in foreignPlayer.Ships)
            {
                //Hit??
                if (s.AllPositions.Contains(field) == true && s.HitPositions.Contains(field) == false)
                {
                    s.HitPositions.Add(field);
                    s.GradeOfDamage++;



                    //Ship finished??
                    if (s.GradeOfDamage == s.Size)
                    {
                        shipFinished = true;
                    }
                    else
                    {
                        shipFinished = false;
                    }
                    hit = true;
                    break;
                }
                else
                    hit = false;

            }
            foreach (Ship s in foreignPlayer.Ships)
            {
                //Game finished?
                if (s.GradeOfDamage == s.Size)
                {
                    countOfHitShips++;
                }
            }

            if (countOfHitShips >= 10)
                gameFinished = true;
            else
                gameFinished = false;

            tries++;

            return hit;
        }


    }
}
