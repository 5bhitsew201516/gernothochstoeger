﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Spieler1
{
    class Program
    {
        static void Main(string[] args)
        {
            KeyValuePair<string, string>[,] battlefield = new KeyValuePair<string, string>[10, 10];

            KeyValuePair<string, string> help = new KeyValuePair<string, string>();
            char omega = 'j';
            for (int i = 0; i < 10; i++)
            {
                char alpha = 'a';

                int j = 0;

                while (alpha <= omega)
                {
                    help = new KeyValuePair<string, string>(alpha + (i + 1).ToString(), "-");
                    battlefield.SetValue(help, i, j);
                    alpha++;
                    j++;
                }
            }
            List<string> selectedFields = new List<string>();
            int tries = 0;
            Request r = null;
            TcpClient client = new TcpClient("localhost", 2055);
            try
            {
                Stream s = client.GetStream();
                StreamReader sr = new StreamReader(s);
                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;
                string startRequest = sr.ReadLine();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(startRequest);
                r = new Request(doc, "");
                bool checkInput = true;

                while (true)
                {
                    if (checkInput == true)
                    {
                        Console.WriteLine("    A  B  C  D  E  F  G  H  I  J");
                        Console.WriteLine();
                        for (int i = 0; i < 10; i++)
                        {
                            if (i < 9)
                                Console.Write(" " + (i + 1));
                            else
                                Console.Write(i + 1);
                            for (int j = 0; j < 10; j++)
                            {
                                Console.Write("  " + battlefield[i, j].Value);
                            }
                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine("\nFalsche Eingabe!");
                    }
                    Console.WriteLine();
                    if (r.gameFinished == true)
                    {
                        Console.WriteLine("Du hast das Spiel mit" + r.tries + "gewonnen");
                    }
                    else
                    {
                        if (r.hit == true)
                        {
                            Console.WriteLine("Du hast einen Treffer gelandet!");
                            if (r.shipFinished == true)
                            {
                                Console.WriteLine("Du hast ein Schiff zerstört");
                            }
                        }
                        else
                        {
                            if (r.tries > 0)
                                Console.WriteLine("Leider daneben!");
                        }
                        if (r.tries > 0)
                            Console.WriteLine("Bisherige Versuche: " + r.tries);
                        Console.WriteLine("Welches Feld soll attackiert werden?");
                        string field = Console.ReadLine();
                        field.ToLower();
                        int number;
                        char letter = ' ';
                        if (Char.IsLetter(field[0]) && Int32.TryParse(field.Remove(0, 1), out number))
                        {
                            letter = field[0];
                            if ((number >= 1 && number <= 10) && (letter >= 'a' && letter <= 'j'))
                            {
                                checkInput = true;
                                for (int i = 0; i < selectedFields.Count; i++)
                                {
                                    if (selectedFields[i] == field)
                                    {
                                        checkInput = false;
                                    }
                                }

                                if (checkInput == true)
                                {
                                    selectedFields.Add(field);
                                    sw.WriteLine("<Response><GameID>" + r.GameID + "</GameID><Position>" + field + "</Position><Tries>" + tries + "</Tries></Response>");

                                    string x = sr.ReadLine();
                                    doc.LoadXml(x);
                                    r = new Request(doc, field);

                                    for (int i = 0; i < 10; i++)
                                    {
                                        for (int j = 0; j < 10; j++)
                                        {
                                            if (battlefield[i, j].Key == field)
                                            {
                                                if (r.hit == true)
                                                {
                                                    help = new KeyValuePair<string, string>(field, "X");
                                                }
                                                else
                                                {
                                                    help = new KeyValuePair<string, string>(field, "0");
                                                }
                                                battlefield.SetValue(help, i, j);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                checkInput = false;
                            }
                        }
                        else
                        {
                            checkInput = false;
                        }
                    }
                }
                Console.ReadKey();
            }
            finally
            {
                client.Close();
            }
        }
    }
}




