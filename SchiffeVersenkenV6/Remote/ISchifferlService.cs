﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remote
{
    public interface ISchifferlService
    {
        void setShips(Dictionary<string, List<string>> Ships, out int playerID);
        bool hitField(string field, ref int tries, out bool gameFinished, out bool shipFinished, int id);
    }
}
