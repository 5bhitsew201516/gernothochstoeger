﻿namespace Spieler2
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button13 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.button26 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.button28 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.button30 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 54);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(507, 500);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(539, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Setzfeld";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 557);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Züge";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 557);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Schlachtfeld";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(542, 54);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(519, 500);
            this.panel2.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Location = new System.Drawing.Point(1158, 26);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(168, 55);
            this.panel3.TabIndex = 8;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(111, 8);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(46, 40);
            this.button3.TabIndex = 2;
            this.button3.Text = "@";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(60, 8);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(45, 40);
            this.button2.TabIndex = 1;
            this.button2.Text = "@";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(45, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "@";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button4);
            this.panel4.Controls.Add(this.button5);
            this.panel4.Controls.Add(this.button6);
            this.panel4.Enabled = false;
            this.panel4.Location = new System.Drawing.Point(1158, 84);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(169, 53);
            this.panel4.TabIndex = 9;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(111, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(46, 40);
            this.button4.TabIndex = 5;
            this.button4.Text = "@";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(61, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(44, 40);
            this.button5.TabIndex = 4;
            this.button5.Text = "@";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(10, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(45, 40);
            this.button6.TabIndex = 3;
            this.button6.Text = "@";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.button7);
            this.panel5.Controls.Add(this.button8);
            this.panel5.Controls.Add(this.button11);
            this.panel5.Enabled = false;
            this.panel5.Location = new System.Drawing.Point(1158, 143);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(169, 54);
            this.panel5.TabIndex = 10;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(114, 5);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(46, 40);
            this.button7.TabIndex = 5;
            this.button7.Text = "@";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(60, 5);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(45, 40);
            this.button8.TabIndex = 4;
            this.button8.Text = "@";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(5, 4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(45, 42);
            this.button9.TabIndex = 3;
            this.button9.Text = "@";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.button13);
            this.panel6.Controls.Add(this.button10);
            this.panel6.Controls.Add(this.button14);
            this.panel6.Controls.Add(this.button9);
            this.panel6.Enabled = false;
            this.panel6.Location = new System.Drawing.Point(1115, 200);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(211, 58);
            this.panel6.TabIndex = 11;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(158, 3);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(44, 43);
            this.button13.TabIndex = 4;
            this.button13.Text = "@";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(107, 4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(45, 42);
            this.button10.TabIndex = 3;
            this.button10.Text = "@";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(10, 5);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(44, 42);
            this.button11.TabIndex = 2;
            this.button11.Text = "@";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(154, 4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(45, 42);
            this.button12.TabIndex = 1;
            this.button12.Text = "@";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.button12);
            this.panel7.Controls.Add(this.button19);
            this.panel7.Controls.Add(this.button18);
            this.panel7.Controls.Add(this.button22);
            this.panel7.Enabled = false;
            this.panel7.Location = new System.Drawing.Point(1115, 264);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(211, 58);
            this.panel7.TabIndex = 12;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(51, 6);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(45, 42);
            this.button17.TabIndex = 4;
            this.button17.Text = "@";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(54, 1);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(45, 42);
            this.button16.TabIndex = 3;
            this.button16.Text = "@";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(196, 7);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(45, 42);
            this.button15.TabIndex = 2;
            this.button15.Text = "@";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(56, 4);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(45, 42);
            this.button14.TabIndex = 1;
            this.button14.Text = "@";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.button23);
            this.panel8.Controls.Add(this.button17);
            this.panel8.Controls.Add(this.button15);
            this.panel8.Controls.Add(this.button20);
            this.panel8.Controls.Add(this.button21);
            this.panel8.Enabled = false;
            this.panel8.Location = new System.Drawing.Point(1078, 328);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(248, 63);
            this.panel8.TabIndex = 13;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(3, 6);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(45, 43);
            this.button21.TabIndex = 4;
            this.button21.Text = "@";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(103, 3);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(45, 43);
            this.button22.TabIndex = 5;
            this.button22.Text = "@";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(98, 6);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(45, 43);
            this.button20.TabIndex = 3;
            this.button20.Text = "@";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(5, 3);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(45, 43);
            this.button19.TabIndex = 2;
            this.button19.Text = "@";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(56, 4);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(41, 42);
            this.button18.TabIndex = 1;
            this.button18.Text = "@";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.button27);
            this.panel9.Controls.Add(this.button25);
            this.panel9.Enabled = false;
            this.panel9.Location = new System.Drawing.Point(1091, 397);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(111, 60);
            this.panel9.TabIndex = 14;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(54, 8);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(45, 41);
            this.button24.TabIndex = 7;
            this.button24.Text = "@";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(5, 5);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(45, 42);
            this.button25.TabIndex = 8;
            this.button25.Text = "@";
            this.button25.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.button16);
            this.panel10.Controls.Add(this.button26);
            this.panel10.Enabled = false;
            this.panel10.Location = new System.Drawing.Point(1217, 400);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(109, 57);
            this.panel10.TabIndex = 15;
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(3, 3);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(45, 41);
            this.button26.TabIndex = 9;
            this.button26.Text = "@";
            this.button26.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(144, 7);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(46, 42);
            this.button23.TabIndex = 6;
            this.button23.Text = "@";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.button24);
            this.panel11.Controls.Add(this.button28);
            this.panel11.Enabled = false;
            this.panel11.Location = new System.Drawing.Point(1091, 463);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(111, 63);
            this.panel11.TabIndex = 16;
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(3, 7);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(45, 42);
            this.button28.TabIndex = 1;
            this.button28.Text = "@";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(56, 6);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(45, 40);
            this.button27.TabIndex = 18;
            this.button27.Text = "@";
            this.button27.UseVisualStyleBackColor = true;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.button30);
            this.panel12.Controls.Add(this.button29);
            this.panel12.Enabled = false;
            this.panel12.Location = new System.Drawing.Point(1219, 466);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(107, 60);
            this.panel12.TabIndex = 17;
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(0, 4);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(48, 41);
            this.button30.TabIndex = 20;
            this.button30.Text = "@";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(54, 4);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(46, 41);
            this.button29.TabIndex = 19;
            this.button29.Text = "@";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 598);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Schiffe versenken";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button29;
    }
}

