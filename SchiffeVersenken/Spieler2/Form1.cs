﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Spieler2
{
    public partial class Form1 : Form
    {
        static int tries = 0;
        static string field = "";
        static TcpClient client = new TcpClient("localhost", 2055);
        static Stream s = client.GetStream();
        static StreamReader sr = new StreamReader(s);
        static StreamWriter sw = new StreamWriter(s);
        static XmlDocument doc = new XmlDocument();
        static Request r = null;

        public Form1()
        {
            sw.AutoFlush = true;
            InitializeComponent();

            string x = sr.ReadLine();
            doc.LoadXml(x);
            r = new Request(doc, "");
            List<Button> field = new List<Button>();
            Button b1;
            Button b2;
            char omega = 'j';
            for (int i = 1; i < 11; i++)
            {
                char alpha = 'a';
                int j = 0;
                Label l = new Label();
                l.Text = "" + i;
                l.Top = 25 + (i * 50);
                l.Left = 20;
                l.Height = 20;
                l.Width = 20;
                Controls.Add(l);
                while (alpha <= omega)
                {
                    Label l1 = new Label();
                    l1.Text = Convert.ToString(Char.ToUpper(alpha));
                    l1.Top = 30;
                    l1.Left = 65 + (j) * 50;
                    l1.Height = 20;
                    l1.Width = 20;

                    Controls.Add(l1);

                    b1 = new Button();
                    b1.BackColor = Color.LightBlue;
                    b1.Click += button_Click;
                    b1.Name = (alpha++) + i.ToString();
                    b1.Height = 49;
                    b1.Width = 49;

                    b1.Top = 0 + (i - 1) * 50;
                    b1.Left = 0 + (j) * 50;
                    field.Add(b1);
                    panel1.Controls.Add(b1);
                    j++;
                }
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            
            if(b.Text != "X" && b.Text != "0")
            {
                field = b.Name.ToString();
                sw.WriteLine("<Response><GameID>" + r.GameID + "</GameID><Position>" + field + "</Position><Tries>" + tries + "</Tries></Response>");
                string x = sr.ReadLine();
                doc.LoadXml(x);
                r = new Request(doc, field);
                if (r.hit == true)
                    b.BackColor = Color.Red;
                else
                    b.BackColor = Color.Blue;

                if (r.gameFinished)
                    MessageBox.Show("Spiel in" + r.tries + "gewonnen!");
                else
                {
                    if (r.shipFinished == true)
                    {
                        MessageBox.Show("Ein Schiff wurde zerstört!");
                    }
                }
                label3.Text = "" + r.tries;
                tries = r.tries;
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Form1_FormClosing(object sedner, EventArgs e)
        {
            s.Close();
            client.Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
