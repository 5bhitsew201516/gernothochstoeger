﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Spieler2
{
    class Request
    {
        public int GameID;
        public string Position;
        public bool hit = false;
        public bool gameFinished = false;
        public bool shipFinished = false;
        public int tries;

        public Request(XmlDocument doc, string field)
        {
            if (doc.SelectSingleNode("Request/Action").InnerText == "Start")
            {
                this.GameID = Convert.ToInt32(doc.SelectSingleNode("Request/GameID").InnerText);
            }
            else if (doc.SelectSingleNode("Request/Action").InnerText == "End")
            {
                gameFinished = true;
            }
            else
            {
                switch (doc.SelectSingleNode("Request/Hit").InnerText)
                {
                    case "True":
                        hit = true;
                        break;
                    case "False":
                        hit = false;
                        break;
                }
                switch (doc.SelectSingleNode("Request/ShipFinished").InnerText)
                {
                    case "True":
                        shipFinished = true;
                        break;
                    case "False":
                        shipFinished = false;
                        break;
                }
                tries = Convert.ToInt32(doc.SelectSingleNode("Request/Tries").InnerText);
            }
        }
    }
}
