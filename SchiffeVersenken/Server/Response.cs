﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Server
{
    class Response
    {
        public string Answer { get; set; }
        public Response(string res, List<Ship> ships, int gameID)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(res);
            string position = doc.SelectSingleNode("Response/Position").InnerText;
            int tries = Convert.ToInt32(doc.SelectSingleNode("Response/Tries").InnerText);
            tries++;
            string answer = "<Request><GameID>" + gameID + "</GameID><Tries>" + tries + "</Tries>";
            int countOfHitShips = 0;
            bool nothit = true;
            for (int i = 0; i < ships.Count; i++)
            {
                //Hit??
                if (ships[i].AllPositions.Contains(position) == true && ships[i].HitPositions.Contains(position) == false)
                {
                    ships[i].HitPositions.Add(position);
                    ships[i].GradeOfDamage++;

                    //Ship finished??
                    if (ships[i].GradeOfDamage == ships[i].Size)
                    {
                        answer = answer + "<Hit>True</Hit><ShipFinished>True</ShipFinished>";
                    }
                    else
                    {
                        answer = answer + "<Hit>True</Hit><ShipFinished>False</ShipFinished>";
                    }
                    nothit = false;
                    break;
                }
                else
                    nothit = true;

                //Game finished?
                if (ships[i].GradeOfDamage == ships[i].Size)
                {
                    countOfHitShips++;
                }
            }
            if (nothit == true)
                answer = answer + "<Hit>False</Hit><ShipFinished>False</ShipFinished>";

            if (countOfHitShips >= 10)
                answer = answer + ("<Action>End</Action>");
            else
                answer = answer + ("<Action>Play</Action>");
            this.Answer = answer + "</Request>";
        }
    }
}
