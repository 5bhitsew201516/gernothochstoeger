﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Submarine:Ship
    {
        public Submarine(List<string> allp) : base(allp)
        {
            this.Size = 2;
            this.AllPositions = allp;
        }
    }
}
