﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Spieler2
{
    public partial class Form1 : Form
    {
        static Dictionary<string, string[]> Ships = new Dictionary<string, string[]>();
        static int helpSub = 0;
        static bool dragdrop = false;
        static int SettingCounter = 0;
        static int tries = 0;
        static string field = "";
        static TcpClient client = new TcpClient("localhost", 2055);
        static Stream s = client.GetStream();
        static StreamReader sr = new StreamReader(s);
        static StreamWriter sw = new StreamWriter(s);
        static XmlDocument doc = new XmlDocument();
        static Request r = null;

        public Form1()
        {
            sw.AutoFlush = true;
            InitializeComponent();

            panel1.Enabled = false;
            Ships.Add("3er 1", new string[3]);

            string x = sr.ReadLine();
            doc.LoadXml(x);
            r = new Request(doc, "");
            List<Button> field = new List<Button>();
            List<Button> field2 = new List<Button>();
            Button b1;
            Button b2;
            char omega = 'j';
            for (int i = 1; i < 11; i++)
            {
                char alpha = 'a';
                int j = 0;
                Label l = new Label();
                l.Text = "" + i;
                l.Top = 25 + (i * 50);
                l.Left = 20;
                l.Height = 20;
                l.Width = 20;

                Label l3 = new Label();
                l.Text = "" + i;
                l.Top = 25 + (i * 50);
                l.Left = panel1.Width + 80;
                l.Height = 20;
                l.Width = 20;
                Controls.Add(l);
                Controls.Add(l3);
                while (alpha <= omega)
                {
                    Label l1 = new Label();
                    l1.Text = Convert.ToString(Char.ToUpper(alpha));
                    l1.Top = 30;
                    l1.Left = 65 + (j) * 50;
                    l1.Height = 20;
                    l1.Width = 20;

                    Label l2 = new Label();
                    l2.Text = Convert.ToString(Char.ToUpper(alpha));
                    l2.Top = 30;
                    l2.Left = panel1.Width + 120 + (j) * 50;
                    l2.Height = 20;
                    l2.Width = 20;

                    Controls.Add(l1);
                    Controls.Add(l2);

                    b1 = new Button();
                    //b1.BackColor = Color.LightBlue;
                    b1.Click += button_Click;
                    b1.Name = (alpha) + i.ToString();
                    b1.Height = 49;
                    b1.Width = 49;

                    b2 = new Button();
                    //b1.BackColor = Color.LightBlue;
                    //b1.Click += button_Click;
                    b2.Name = (alpha) + i.ToString();
                    b2.Height = 49;
                    b2.Width = 49;
                    b2.AllowDrop = true;
                    b2.DragDrop += button_DragDrop;
                    b2.DragEnter += button_DragEnter;

                    alpha++;

                    b1.Top = 0 + (i - 1) * 50;
                    b1.Left = 0 + (j) * 50;

                    b2.Top = 0 + (i - 1) * 50;
                    b2.Left = 0 + (j) * 50;
                    field.Add(b1);
                    field2.Add(b2);
                    panel1.Controls.Add(b1);
                    panel2.Controls.Add(b2);
                    j++;
                }
            }
            button1.AllowDrop = true;
            button2.AllowDrop = true;
            button3.AllowDrop = true;
            button6.AllowDrop = true;
            button5.AllowDrop = true;
            button4.AllowDrop = true;
            button9.AllowDrop = true;
            button8.AllowDrop = true;
            button7.AllowDrop = true;
            button12.AllowDrop = true;
            button11.AllowDrop = true;
            button10.AllowDrop = true;
            button13.AllowDrop = true;
            button14.AllowDrop = true;
            button15.AllowDrop = true;
            button16.AllowDrop = true;
            button17.AllowDrop = true;
            button18.AllowDrop = true;
            button19.AllowDrop = true;
            button20.AllowDrop = true;
            button21.AllowDrop = true;
            button22.AllowDrop = true;
            button23.AllowDrop = true;
            button24.AllowDrop = true;
            button25.AllowDrop = true;
            button26.AllowDrop = true;
            button27.AllowDrop = true;
            button28.AllowDrop = true;
            button29.AllowDrop = true;
            button30.AllowDrop = true;

            button1.MouseDown += button_MouseDown;
            button2.MouseDown += button_MouseDown;
            button3.MouseDown += button_MouseDown;
            button6.MouseDown += button_MouseDown;
            button5.MouseDown += button_MouseDown;
            button4.MouseDown += button_MouseDown;
            button9.MouseDown += button_MouseDown;
            button8.MouseDown += button_MouseDown;
            button7.MouseDown += button_MouseDown;
            button12.MouseDown += button_MouseDown;
            button11.MouseDown += button_MouseDown;
            button10.MouseDown += button_MouseDown;
            button13.MouseDown += button_MouseDown;
            button14.MouseDown += button_MouseDown;
            button15.MouseDown += button_MouseDown;
            button16.MouseDown += button_MouseDown;
            button17.MouseDown += button_MouseDown;
            button18.MouseDown += button_MouseDown;
            button19.MouseDown += button_MouseDown;
            button20.MouseDown += button_MouseDown;
            button21.MouseDown += button_MouseDown;
            button22.MouseDown += button_MouseDown;
            button23.MouseDown += button_MouseDown;
            button24.MouseDown += button_MouseDown;
            button25.MouseDown += button_MouseDown;
            button26.MouseDown += button_MouseDown;
            button27.MouseDown += button_MouseDown;
            button28.MouseDown += button_MouseDown;
            button29.MouseDown += button_MouseDown;
            button30.MouseDown += button_MouseDown;
        }

        private void button_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void button_DragDrop(object sender, DragEventArgs e)
        {
            if(e.Data.GetData(DataFormats.Text).ToString() !="")
            {
                Button b = (Button)sender;
                bool checkField = false;
                if (b.Text != "@")
                {
                    if ((SettingCounter - helpSub) == 0)
                    {
                        Ships.Values.Last()[SettingCounter - helpSub] = b.Name;
                        b.Text = e.Data.GetData(DataFormats.Text).ToString();
                        SettingCounter++;
                        checkField = true;
                    }
                    else
                    {
                        //Zerlegung der Felder in Zahlen und Buchstaben
                        char actLetter = b.Name.ToString()[0];
                        int actNumber;
                        if (b.Name.Length >= 3)
                            actNumber = Convert.ToInt32(b.Name.ToString().Substring(1, 2));
                        else
                            actNumber = Convert.ToInt32(b.Name.ToString().Substring(1, 1));

                        char helpLetter1 = Ships.Values.Last()[SettingCounter - (helpSub + 1)][0];
                        char helpLetter2=' ';
                        char helpLetter3=' ';
                        char helpLetter4=' ';
                        if ((SettingCounter-helpSub) >= 2)
                        {
                            helpLetter2 = Ships.Values.Last()[SettingCounter - (helpSub+2)][0];
                            if ((SettingCounter-helpSub) >= 3)
                            {
                                helpLetter3 = Ships.Values.Last()[SettingCounter - (helpSub + 3)][0];
                                if ((SettingCounter-helpSub) >= 4)
                                {
                                    helpLetter4 = Ships.Values.Last()[SettingCounter - (helpSub + 4)][0];
                                }
                            }
                        }

                        int helpNumber1 = 0;
                        int helpNumber2 = 0;
                        int helpNumber3 = 0;
                        int helpNumber4 = 0;
                        if (Ships.Values.Last()[SettingCounter - (helpSub + 1)].Length >= 3)//für 10
                        {
                            helpNumber1 = 10;
                            if ((SettingCounter - helpSub) >= 2 && Ships.Values.Last()[SettingCounter - (helpSub)].Length >= 3)
                            {
                                helpNumber2 = 10;
                                if ((SettingCounter - helpSub) >= 3)
                                {
                                    helpNumber3 = 10;
                                    if ((SettingCounter - helpSub) >= 4)
                                    {
                                        helpNumber4 = 10;
                                    }
                                }
                            }
                        }
                        else //1-9
                            helpNumber1 = Convert.ToInt32(Ships.Values.Last()[SettingCounter - (helpSub + 1)].Substring(1, 1));
                        if ((SettingCounter-helpSub) >= 2)
                        {
                            if (Ships.Values.Last()[SettingCounter - (helpSub+2)].Length < 3)
                            {
                                helpNumber2 = Convert.ToInt32(Ships.Values.Last()[SettingCounter - (helpSub+2)].Substring(1, 1));
                            }
                            else
                            {
                                helpNumber2 = 10;
                            }
                            if ((SettingCounter-helpSub) >= 3)
                            {
                                helpNumber3 = Convert.ToInt32(Ships.Values.Last()[SettingCounter - (helpSub +3)].Substring(1, 1));
                                if ((SettingCounter-helpSub) >= 4)
                                {
                                    helpNumber4 = Convert.ToInt32(Ships.Values.Last()[SettingCounter - (helpSub +4)].Substring(1, 1));
                                }
                            }
                        }
                        //Überprüfen ob Auswahl gültig
                        if ((SettingCounter-helpSub) == 1)
                        {
                            if ((helpLetter1 == actLetter && (actNumber == (helpNumber1 + 1) || actNumber == (helpNumber1 - 1))) || (helpNumber1 == actNumber && (actLetter == (helpLetter1 + 1) || (actLetter == (helpLetter1 - 1)))))
                            {
                                checkField = true;
                            }
                        }
                        if((SettingCounter-helpSub)==2)
                        {
                            if(helpLetter1 == helpLetter2 && helpLetter2 == actLetter && (actNumber ==(helpNumber1+1) || actNumber==(helpNumber1-1)|| actNumber==(helpNumber2+1) || actNumber == (helpNumber2-1)))
                            {
                                checkField=true;
                            }
                            if(helpNumber1==helpNumber2 && helpNumber2 == actNumber && (actLetter==(helpLetter1+1)|| actLetter == (helpLetter1 - 1) || actLetter ==(helpLetter2+1) || actLetter == (helpLetter2-1)))
                            {
                                checkField = true;
                            }
                        }
                        if ((SettingCounter - helpSub) == 3)
                        {
                            if (helpLetter1 == helpLetter2 && helpLetter2 == actLetter && (actNumber == (helpNumber1 + 1) || actNumber == (helpNumber1 - 1) || actNumber == (helpNumber2 + 1) || actNumber == (helpNumber2 - 1)||actNumber==(helpNumber3+1)||actNumber==(helpNumber3-1)))
                            {
                                checkField = true;
                            }
                            if (helpNumber1 == helpNumber2 && helpNumber2 == actNumber && (actLetter == (helpLetter1 + 1) || actLetter==(helpLetter1-1)|| actLetter == (helpLetter2 + 1) || actLetter == (helpLetter2 - 1) || actLetter == (helpLetter3 +1) || actLetter == (helpLetter3 - 1)))
                            {
                                checkField = true;
                            }
                        }
                        if ((SettingCounter - helpSub) == 4)
                        {
                            if (helpLetter1 == helpLetter2 && helpLetter2 == actLetter && (actNumber == (helpNumber1 + 1) || actNumber == (helpNumber1 - 1) || actNumber == (helpNumber2 + 1) || actNumber == (helpNumber2 - 1) || actNumber == (helpNumber3 + 1) || actNumber == (helpNumber3 - 1) || actNumber == (helpNumber4 + 1) || actNumber == (helpNumber4 - 1)))
                            {
                                checkField = true;
                            }
                            if (helpNumber1 == helpNumber2 && helpNumber2 == actNumber && (actLetter == (helpLetter1 + 1) || actLetter == (helpLetter1 - 1) || actLetter == (helpLetter2 + 1) || actLetter == (helpLetter2 - 1) || actLetter == (helpLetter3 + 1) || actLetter == (helpLetter3 - 1) || actLetter == (helpLetter4 + 1) || actLetter == (helpLetter4 - 1)))
                            {
                                checkField = true;
                            }
                        }


                        if (checkField == true)
                        {
                            Ships.Values.Last()[SettingCounter - helpSub] = b.Name;
                            b.Text = e.Data.GetData(DataFormats.Text).ToString();
                            SettingCounter++;
                        }
                        else
                            MessageBox.Show("Feld kann nicht gesetzt werden!");
                    }

                    if (checkField == true)
                    {
                        dragdrop = true;

                        switch (SettingCounter)
                        {
                            case 3:
                                panel3.Enabled = false;
                                panel4.Enabled = true;
                                helpSub = SettingCounter;
                                Ships.Add("3er 2", new string[3]);
                                break;
                            case 6:
                                panel4.Enabled = false;
                                panel5.Enabled = true;
                                helpSub = SettingCounter;
                                Ships.Add("3er 3", new string[3]);
                                break;
                            case 9:
                                panel5.Enabled = false;
                                panel6.Enabled = true;
                                helpSub = SettingCounter;
                                Ships.Add("4er 1", new string[4]);
                                break;
                            case 13:
                                panel6.Enabled = false;
                                panel7.Enabled = true;
                                helpSub = SettingCounter;
                                Ships.Add("4er 2", new string[4]);
                                break;
                            case 17:
                                panel7.Enabled = false;
                                panel8.Enabled = true;
                                helpSub = SettingCounter;
                                Ships.Add("5er 1", new string[5]);
                                break;
                            case 22:
                                panel8.Enabled = false;
                                panel9.Enabled = true;
                                helpSub = SettingCounter;
                                Ships.Add("2er 1", new string[2]);
                                break;
                            case 24:
                                panel9.Enabled = false;
                                panel10.Enabled = true;
                                helpSub = SettingCounter;
                                Ships.Add("2er 2", new string[2]);
                                break;
                            case 26:
                                panel10.Enabled = false;
                                panel11.Enabled = true;
                                helpSub = SettingCounter;
                                Ships.Add("2er 3", new string[2]);
                                break;
                            case 28:
                                panel11.Enabled = false;
                                panel12.Enabled = true;
                                helpSub = SettingCounter;
                                Ships.Add("2er 4", new string[2]);
                                break;
                            case 30:
                                panel12.Enabled = false;
                                panel2.Enabled = false;
                                helpSub = 0;
                                //Methode aufrufen, die zuständig ist für das Erstellen eines String in XML-Format und diesen an Server weiterleitet 
                                break;
                        }
                    }
                }
                else
                    MessageBox.Show("Ein Feld kann nicht 2x gesetzt werden!");
            }
        }

        private void button_MouseDown(object sender, MouseEventArgs e)
        {
            Button b = (Button)sender;
            b.DoDragDrop(b.Text, DragDropEffects.Move);
            if (dragdrop == true)
                b.Enabled = false;
            dragdrop = false;
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            
            //if(b.BackColor != Color.Red && b.BackColor != Color.Blue)
            if(b.Text != "X" && b.Text != "0")
            {
                field = b.Name.ToString();
                sw.WriteLine("<Response><GameID>" + r.GameID + "</GameID><Position>" + field + "</Position><Tries>" + r.tries + "</Tries></Response>");
                string x = sr.ReadLine();
                doc.LoadXml(x);
                r = new Request(doc, field);
                if (r.hit == true)
                    b.BackColor = Color.Red;
                else
                    b.BackColor = Color.Blue;

                if (r.gameFinished)
                    MessageBox.Show("Spiel in" + r.tries + "gewonnen!");
                else
                {
                    if (r.shipFinished == true)
                    {
                        MessageBox.Show("Ein Schiff wurde zerstört!");
                    }
                }
                label3.Text = "" + r.tries;
                tries = r.tries;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Form1_FormClosing(object sedner, EventArgs e)
        {
            s.Close();
            client.Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
