﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplex
{
    class Simplex
    {
        bool finished;
        int anzx;
        int anzs;
        int anzxs;
        int anzz;
        double[,] zahlen;
        double[] rs;
        double[] q;
        string[] bezlinks;
        string[] bezoben;
        int pspalte;
        int pzeile;
        double pelement;
        double[] pspaltenwerte;

        public Simplex(int anzEnd, int anzAus)
        {
            this.finished = false;
            this.anzx = anzEnd;
            this.anzs = anzAus;
            this.anzxs = anzAus + anzEnd;
            this.anzz = anzs + 1;
            this.zahlen = new double[anzz, anzxs];
            this.rs = new double[anzz];
            this.q = new double[anzz-1];
            this.pspaltenwerte = new double[anzz];

            for (int i = 0; i < anzz; i++)
            {
                fillLine(i, new double[] { 0, 0, 0, 0 });
            }
            this.bezlinks = new string[anzz];
            for (int i = 0; i < anzz; i++)
            {
                if (i == 0)
                    bezlinks[i] = "Z";
                else
                    bezlinks[i] = "S"+i;
            }
            this.bezoben = new string[anzxs + 2];
            for (int i = 0; i < (anzxs+2); i++)
            {
                if (i <= anzx)
                    bezoben[i - 1] = "X" + 1;
                else
                {
                    if (i <= anzxs)
                        bezoben[i - 1] = "S" + (i-anzx);
                    if (i <= (anzxs+1))
                        bezoben[i - 1] = "RS";
                    if (i <= (anzxs+2))
                        bezoben[i - 1] = "Q" + 1;
                }
            }
        }

        public void fillLine(int z, double[] werte)
        {
            for (int i = 0; i < anzx; i++)
            {
                zahlen[z, i] = werte[i];
            }

            for (int i = anzx; i < anzxs; i++)
            {
                if (z != 0)
                {
                    if (z == (i - anzx + 1))
                        zahlen[z, i] = 1;
                    else
                        zahlen[z, i] = 0;
                }
                else
                    zahlen[z, i] = 0;
            }
            rs[z] = werte[werte.Length - 1];
            if (z != 0)
                q[z - 1] = 0;
        }

        public void quotientenBerechnen()
        {
            double max = Double.MinValue;
            for (int i = 0; i < anzx; i++)
            {
                if(max < zahlen[0,i])
                {
                    max = zahlen[0, i];
                    pspalte = i;
                }
            }
            Console.WriteLine("PivotSpalte: " +max+ "in Spalte: " + pspalte);

            if (max == 0)
                finished = true;

            for (int i = 0; i < anzz; i++)
            {
                q[i - 1] = rs[i] / zahlen[i, pspalte];
            }

            double max2 = Double.MaxValue;
            for (int i = 0; i < q.Length; i++)
            {
                if(max2>q[i])
                {
                    max2 = q[i];
                    pzeile = i + 1;
                }
            }
            Console.WriteLine("Pivotzeile: " + max2 + "in Zeile" +pzeile);

            for (int i = 0; i < anzz; i++)
            {
                pspaltenwerte[i] = zahlen[i, pspalte];
            }

            pelement = zahlen[pzeile, pspalte];
            Console.WriteLine("Pivotelement: " +pelement);
        }

        public void pivotdividieren()
        {
            for (int i = 0; i < anzxs; i++)
            {
                zahlen[pzeile, i] /= pelement;
            }
            rs[pzeile] /= pelement;
            for (int i = 0; i < anzz; i++)
            {
                if (i != pzeile)
                {
                    for (int j = 0; j < anzxs; j++)
                    {
                        zahlen[i, j] = zahlen[i, j] - pspaltenwerte[i] * zahlen[pzeile, j];
                    }
                    rs[i] = rs[i] - pspaltenwerte[i] * rs[pzeile];
                }
            }
            bezlinks[pzeile] = "X" + (pspalte + 1);
        }

        public bool getState()
        {
            return finished;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\t");

            foreach (var item in bezoben)
            {
                sb.Append(item + "\t");
            }
            sb.Append("\n");

            for (int i = 0; i < anzz; i++)
            {
                sb.Append(bezlinks[i] + "\t");
                for (int j = 0; j < anzxs; j++)
                {
                    sb.Append(zahlen[i, j].ToString("0.00") + "\t");
                }
                sb.Append(rs[i].ToString("0.00") + "\t");
                if(finished==false)
                {
                    if (i != 0)
                        sb.Append(q[i - 1].ToString("0.00"));
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }


    }
}
