﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplex
{
    class Program
    {
        static SimplexContext.SimplexDataContext sdc = new SimplexContext.SimplexDataContext();

        static void Main(string[] args)
        {
            var endprodukt = from x in sdc.Produkts select x;
            var ausgangsprodukt = from x in sdc.Zutats select z;

            var zwischentabelle = from y in sdc.ProduktHasZutats select y;

            Simplex s = new Simplex(endprodukt.Count(), ausgangsprodukt.Count());

            int[] produkte = new int[endprodukt.Count()];
            int[] zutaten = new int[ausgangsprodukt.Count()];
            double[,] produktzutaten = new double[endprodukt.Count(), ausgangsprodukt.Count()];

            int i = 0;
            int j = 0;

            foreach (var item in endprodukt)
            {
                produkte[i] = Int32.Parse(item.Produktmenge.ToString());
                i++;
            }
            i = 0;
            foreach (var item in ausgangsprodukt)
            {
                zutaten[i] = Int32.Parse(item.Menge.ToString());
                i++;
            }
            i = 0;

            foreach (var item in zwischentabelle)
            {
                if(j !=0 && (j % produkte.Count()) == 0)
                {
                    i++;
                    j = 0;
                }
                produktzutaten[i, j] = Double.Parse(item.Zutatmenge.ToString());
                j++;
            }
            s.fillLine(0, new double[] { produkte[0], produkte[1], 0 });
            s.fillLine(1, new double[] { produktzutaten[0,0], produktzutaten[1,0], zutaten[0] });
            s.fillLine(2, new double[] { produktzutaten[0,1], produktzutaten[1,1], zutaten[1] });

            s.quotientenBerechnen();
            Console.WriteLine(s.ToString());
            Console.WriteLine("--------------------------------------------------------");
            simplexdurchlaufen(s);
            Console.ReadLine();
        }

        static void simplexdurchlaufen(Simplex s)
        {
            while(s.getState() == false)
            {
                s.pivotdividieren();
                s.quotientenBerechnen();
                Console.WriteLine(s.ToString());
                Console.WriteLine("----------------------------------------------");
            }
        }
    }
}
