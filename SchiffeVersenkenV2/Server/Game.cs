﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Game
    {
        static int nextID = 1;
        public int GameID;
        public List<Ship> Ships { get; set; }
        public User User { get; set; }

        public Game()
        {
            this.GameID = nextID;
            this.Ships = GetRandomShips();
            nextID++;
        }

        public static List<Ship> GetRandomShips()
        {
            List<Ship> helpShips = new List<Ship>();
            List<string> ships = new List<string>();
            #region Adding Ships
            ships.Add("das Schlachschiff(5er)");
            ships.Add("der Kreuzer(4er)");
            ships.Add("der Kreuzer(4er)");
            ships.Add("der Zerstörer(3er)");
            ships.Add("der Zerstörer(3er)");
            ships.Add("der Zerstörer(3er)");
            ships.Add("das U-Boot(2er)");
            ships.Add("das U-Boot(2er)");
            ships.Add("das U-Boot(2er)");
            ships.Add("das U-Boot(2er)");
            #endregion
            Dictionary<int, char> helpdic = new Dictionary<int, char>();
            char alpha = 'a';
            for (int i = 1; i <= 10; i++)
            {
                helpdic.Add(i, alpha);
                alpha++;
            }
            Random r = new Random();
            int number = 0;
            char letter;
            int direction = 0;
            int shiplength = 0;

            for (int i = 0; i < ships.Count; i++)
            {
                direction = r.Next(1, 3);
                switch (i)
                {
                    case 0:
                        shiplength = 5;
                        break;
                    case 1:
                    case 2:
                        shiplength = 4;
                        break;
                    case 3:
                    case 4:
                    case 5:
                        shiplength = 3;
                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        shiplength = 2;
                        break;
                }
                List<string> shipPositions = new List<string>();
                if (direction == 1)
                {
                    number = r.Next(1, 11 - shiplength);
                    letter = helpdic[r.Next(1, 11)];
                    for (int j = 0; j < shiplength; j++)
                    {
                        shipPositions.Add("" + letter + number);
                        number++;
                    }
                }
                else
                {
                    number = r.Next(1, 11);
                    letter = helpdic[r.Next(1, 11 - shiplength)];
                    for (int j = 0; j < shiplength; j++)
                    {
                        shipPositions.Add("" + letter + number);
                        letter++;
                    }
                }
                bool check = true;
                for (int y = 0; y < shipPositions.Count; y++)
                {
                    for (int j = 0; j < helpShips.Count; j++)
                    {
                        if (helpShips[j].AllPositions.Contains(shipPositions[y]))
                        {
                            check = false;
                            break;
                        }
                    }
                }
                if (check == true)
                {
                    switch (shiplength)
                    {
                        case 5:
                            helpShips.Add(new Battleship(shipPositions));
                            break;
                        case 4:
                            helpShips.Add(new Cruiser(shipPositions));
                            break;
                        case 3:
                            helpShips.Add(new Destroyer(shipPositions));
                            break;
                        case 2:
                            helpShips.Add(new Submarine(shipPositions));
                            break;
                    }
                }
                else
                {
                    i--;
                }
            }
            return helpShips;
        }
    }
}
