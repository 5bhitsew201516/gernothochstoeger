﻿#define LOG
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Server
{
    class Program
    {
        static TcpListener listener;
        const int LIMIT = 5;


        public static void Main(string[] args)
        {
            listener = new TcpListener(2055);
            listener.Start();

#if LOG
            Console.WriteLine("Server mounted, listening to port 2055");
#endif
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }

        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();

#if LOG
                Console.WriteLine("Connected: {0}", soc.RemoteEndPoint);
#endif
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true;

                    Game g = new Game();
                    sw.WriteLine("<Request><GameID>" + g.GameID + "</GameID><Action>Start</Action></Request>");
                    while(true)
                    {
                        string response = sr.ReadLine();
                        Console.WriteLine(response);
                        Response res = new Response(response, g.Ships, g.GameID);
                        sw.WriteLine(res.Answer);
                    }
                    s.Close();
                }

                catch (Exception e)
                {
#if LOG
                    Console.WriteLine(e.Message);
#endif
                }

#if LOG
                Console.WriteLine("Disconnected: {0}", soc.RemoteEndPoint);
#endif
                soc.Close();
            }
        }
    }
}

