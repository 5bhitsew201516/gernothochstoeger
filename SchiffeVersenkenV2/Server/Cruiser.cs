﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Cruiser:Ship
    {
        public Cruiser(List<string> allp) : base(allp)
        {
            this.Size = 4;
            this.AllPositions = allp;
        }
    }
}
